import java.util.concurrent.{Executors, TimeUnit}

import com.twitter.algebird.HyperLogLog.{j, _}
import com.twitter.algebird.{HyperLogLogMonoid, Max, SparseHLL}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.{
  Milliseconds,
  StreamingContext,
  Time,
  Duration => SDuration
}
import org.spark_project.guava.util.concurrent.AbstractScheduledService.Scheduler
import org.spark_project.jetty.util.thread

import scala.concurrent.duration._

object Main extends App {

  Logger.getLogger("org").setLevel(Level.WARN)

  val P_BITS_LENGTH = 4

  import proto.Utils._
  import proto.SlidingHLL._

  val conf =
    new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")

  val ssc = new StreamingContext(conf, 1000 millis)
  ssc.checkpoint("checkpoint_dir")

  val linesQueue = scala.collection.mutable.Queue[RDD[String]]()
  val linesStream = ssc.queueStream(linesQueue, true)

  val scheduler = Executors.newScheduledThreadPool(1)
  scheduler.scheduleAtFixedRate(
    new Runnable {
      var i = 0

      override def run(): Unit = {
        linesQueue += ssc.sparkContext.makeRDD(
          {
            val res = (for (j <- 0 to i + 1) yield j.toString).toList
            i = i + 1
            res
          }
        )
      }

    },
    2,
    2,
    SECONDS
  )

  val l = List("1", "2", "3", "1", "2", "3", "4", "5", "6", "7", "8")

  val realNumber = l.foldLeft(Set[String]())((acc, el) => acc + el).size

  println(realNumber)

  linesStream
    .map(_.getBytes)
    .approxCountDistinctByWindow(P_BITS_LENGTH, 10 seconds, 5 seconds)
    .print()

  linesQueue += ssc.sparkContext.makeRDD(l)
//
//
  ssc.start()
  ssc.awaitTermination()
}
