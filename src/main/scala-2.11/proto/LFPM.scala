package proto

sealed abstract class LFPM[T: Ordering](W: Long) extends Serializable { self =>
  val list: List[(Long, T)]

  def add(ts: Long, v: T): LFPM[T] = new LFPM[T](W) {

    import scala.math.Ordering.Implicits._

    override val list: List[(Long, T)] = {
      val r = self.list
        .filterNot(_._1 < ts - W)
        .filterNot(_._2 <= v)

      if (r.exists(x => x._1 >= ts && x._2 > v))
      {
        r
      }
      else
      {
        (ts, v)::r
      }
    }

      (ts, v) :: self.list.filterNot(_._1 < ts - W).filterNot(_._2 <= v)
  }

  def remove(ts: Long, v: T): LFPM[T] = new LFPM[T](W) {

    import scala.math.Ordering.Implicits._

    override val list: List[(Long, T)] = self.list.filterNot(_._1 <= ts)
  }

  def - (l: LFPM[T]): LFPM[T] =
    l.list.foldLeft(this)(
      (acc: LFPM[T], v: (Long, T)) => acc.remove(v._1, v._2))

  def + (l: LFPM[T]): LFPM[T] = {
    l.list.foldLeft(this)((acc: LFPM[T], v: (Long, T)) => acc.add(v._1, v._2))
  }


  def extract(): Option[T] = if (this.list.isEmpty) Option.empty[T] else Option(this.list.maxBy(_._2)._2)


  override def toString: String = list.toString()
}

object LFPM {
  def apply[T: Ordering](W: Long): LFPM[T] = new LFPM[T](W) {
    override val list: List[(Long, T)] = List()
  }

  def of[T: Ordering](W: Long, ts: Long, v: T): LFPM[T] = new LFPM[T](W) {
    override val list: List[(Long, T)] = (ts, v) :: Nil
  }

}
