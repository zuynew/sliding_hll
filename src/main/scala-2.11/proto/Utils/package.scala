package proto

import org.apache.spark.streaming.Milliseconds

import scala.concurrent.duration.FiniteDuration

package object Utils {

  implicit def finiteDuration2SparkDuration(
      d: FiniteDuration): org.apache.spark.streaming.Duration =
    Milliseconds(d.toMillis)

  implicit def string2Bytes(i: String): Array[Byte] = i.getBytes

}
