package proto

import com.twitter.algebird.HyperLogLog.{hash, jRhoW}
import com.twitter.algebird.{HyperLogLogMonoid, Max, SparseHLL}
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.{Time, Duration => SDuration}

object SlidingHLL {

  implicit class SlidingHLLOps(s: DStream[Array[Byte]]) extends Serializable {

    def approxCountDistinctByWindow(bits: Int,
                                    windowDuration: SDuration,
                                    slideDuration: SDuration,
                                    numPartitions: Int =
                                      s.context.sparkContext.defaultParallelism)
      : DStream[Double] = {
      val hll = new HyperLogLogMonoid(bits)

      s.transform((ds: RDD[Array[Byte]], ts: Time) =>
          ds.map((ts.milliseconds, _)))
        .map(x => (x._1, hash(x._2)))
        .map(x0 => {
          val (ts, x) = x0
          val (j, r) = jRhoW(x, bits)
          (j, proto.LFPM.of(windowDuration.milliseconds, ts, r))
        })
        .reduceByKeyAndWindow(
          (x: proto.LFPM[Byte], y: proto.LFPM[Byte]) => {
            x + y
          },
          (x: proto.LFPM[Byte], y: proto.LFPM[Byte]) => {
            x - y
          },
          windowDuration,
          slideDuration,
          numPartitions,
          (x: (Int, proto.LFPM[Byte])) => x._2.list.nonEmpty
        )
        .map(
          {
            case (j, l0) =>
              l0.extract()
                .map(r => SparseHLL(bits, Map(j -> Max(r))))
                .getOrElse(hll.zero)
          }
        )
        .reduce(_ + _)
        .map(_.estimatedSize)
    }
  }
}
