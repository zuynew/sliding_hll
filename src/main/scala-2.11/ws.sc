
  def toBinary(i: Int, digits: Int = 8) =
    String.format("%" + digits + "s", i.toBinaryString).replace(' ', '0')

  toBinary(2097152)
  toBinary(35651584)
  toBinary(5)