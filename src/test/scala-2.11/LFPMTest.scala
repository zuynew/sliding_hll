import org.scalatest._

class LFPMTest extends FlatSpec with Matchers  {

  "LFPM" should "build list of one item" in {
    val W = 10000
    val t = (10000, 10)
    val lfpm = proto.LFPM.of(W, t._1, t._2)

    lfpm.list should be (List(t))
  }

  "LFPM" should "add item with smaller val and bigger ts within window" in {
    val W = 10000
    val t0 = (10000, 10)
    val t1 = (15000, 5)
    val lfpm = proto.LFPM
      .of(W, t0._1, t0._2)
      .add(t1._1, t1._2)


    lfpm.list should be (List(t1, t0))
  }



  "LFPM" should "add item with bigger val and bigger ts within window" in {
    val W = 10000
    val t0 = (10000, 10)
    val t1 = (15000, 15)
    val lfpm = proto.LFPM
      .of(W, t0._1, t0._2)
      .add(t1._1, t1._2)


    lfpm.list should be (List(t1))
  }


  "LFPM" should "add item with smaller val and bigger ts over window" in {
    val W = 10000
    val t0 = (10000, 10)
    val t1 = (21000, 5)
    val lfpm = proto.LFPM
      .of(W, t0._1, t0._2)
      .add(t1._1, t1._2)


    lfpm.list should be (List(t1))
  }


  "LFPM" should "add item with bigger val and bigger ts over window" in {
    val W = 10000
    val t0 = (10000, 10)
    val t1 = (21000, 15)
    val lfpm = proto.LFPM
      .of(W, t0._1, t0._2)
      .add(t1._1, t1._2)


    lfpm.list should be (List(t1))
  }

  "LFPM" should "remove items with ts <= item_ts" in {
    val W = 10000
    val t0 = (10000, 10)
    val t1 = (15000, 5)
    val t2 = (16000, 3)
    val t3 = (17000, 2)
    val lfpm = proto.LFPM
      .of(W, t0._1, t0._2)
      .add(t1._1, t1._2)
      .add(t2._1, t2._2)
      .add(t3._1, t3._2)
      .remove(t1._1, t1._2)


    lfpm.list should be (List(t3, t2))
  }






  "LFPM with values" should "merge with single item LFPM with smaller val and bigger ts within window" in {
    val W = 10000
    val t0 = (10000, 10)
    val t1 = (15000, 5)
    val lfpm = proto.LFPM
      .of(W, t0._1, t0._2) + proto.LFPM.of(W, t1._1, t1._2)


    lfpm.list should be (List(t1, t0))
  }



  "LFPM with values" should "merge with single item LFPM with bigger val and bigger ts within window" in {
    val W = 10000
    val t0 = (10000, 10)
    val t1 = (15000, 15)
    val lfpm = proto.LFPM
      .of(W, t0._1, t0._2) + proto.LFPM.of(W, t1._1, t1._2)


    lfpm.list should be (List(t1))
  }


  "LFPM with values" should "merge with single item LFPM with smaller val and bigger ts over window" in {
    val W = 10000
    val t0 = (10000, 10)
    val t1 = (21000, 5)
    val lfpm = proto.LFPM
      .of(W, t0._1, t0._2) + proto.LFPM.of(W, t1._1, t1._2)


    lfpm.list should be (List(t1))
  }


  "LFPM with values" should "merge with single item LFPM with bigger val and bigger ts over window" in {
    val W = 10000
    val t0 = (10000, 10)
    val t1 = (21000, 15)
    val lfpm = proto.LFPM
      .of(W, t0._1, t0._2) + proto.LFPM.of(W, t1._1, t1._2)


    lfpm.list should be (List(t1))
  }


  "LFPM with values" should "remove items with ts <= item_ts" in {
    val W = 10000
    val t0 = (10000, 10)
    val t1 = (15000, 5)
    val t2 = (16000, 3)
    val t3 = (17000, 2)
    val lfpm = proto.LFPM
      .of(W, t0._1, t0._2)
      .add(t1._1, t1._2)
      .add(t2._1, t2._2)
      .add(t3._1, t3._2) - proto.LFPM.of(W,t1._1, t1._2)


    lfpm.list should be (List(t3, t2))
  }





  "LFPM" should "extract max val item" in {
    val W = 10000
    val t0 = (10000, 10)
    val t1 = (15000, 5)
    val t2 = (16000, 3)
    val t3 = (17000, 2)
    val v = proto.LFPM
      .of(W, t0._1, t0._2)
      .add(t1._1, t1._2)
      .add(t2._1, t2._2)
      .add(t3._1, t3._2)
      .extract()


    v should be (Some(t0._2))
  }


}
