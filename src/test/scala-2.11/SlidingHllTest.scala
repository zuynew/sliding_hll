import com.holdenkarau.spark.testing.StreamingSuiteBase
import com.twitter.algebird._
import org.apache.spark.streaming.Seconds
import org.apache.spark.streaming.dstream.DStream
import org.scalatest.FunSuite
import proto.SlidingHLL._

class SlidingHllTest extends FunSuite with StreamingSuiteBase {

  test("CountByWindow with windowDuration 3s and slideDuration=2s") {
    // There should be 2 windows :  {batch2, batch1},  {batch4, batch3, batch2}

    val BITS = 8

    val hll = new HyperLogLogMonoid(BITS)
    implicit def list2hll[A: Hash128](l: List[A]) = l.foldLeft(hll.zero)(
      (acc: HLL, v: A) => { acc + hll.toHLL(v) }
    )

    val batch1 = List("a", "b")
    val batch2 = List("d", "f", "a")
    val batch3 = List("f", "g", " h")
    val batch4 = List("a")
    val input: List[List[String]] = List(batch1, batch2, batch3, batch4)
    val expected: List[List[Double]] =
      List(List((batch1 ::: batch2).estimatedSize),
           List((batch2 ::: batch3 ::: batch4).estimatedSize))

    def approxCountDistinctByWindow(ds: DStream[String]): DStream[Double] = {
      ds.map(_.getBytes)
        .approxCountDistinctByWindow(BITS,
                                     windowDuration = Seconds(3),
                                     slideDuration = Seconds(2))
    }
    testOperation[String, Double](input,
                                  approxCountDistinctByWindow _,
                                  expected,
                                  ordered = true)
  }

}
