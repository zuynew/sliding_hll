name := "sliding_hll"

version := "1.0"

scalaVersion := "2.11.11"

libraryDependencies += "org.apache.spark" % "spark-streaming_2.11" % "2.2.0"

// https://mvnrepository.com/artifact/com.twitter/algebird-core_2.11
libraryDependencies += "com.twitter" % "algebird-core_2.11" % "0.13.3"
libraryDependencies += "com.holdenkarau" %% "spark-testing-base" % "2.2.0_0.7.4" % "test"